package Imagenes;

import Clases.CustomImageIcon;
import Clases.ModelTablePersonal;
import Conexion.ConexionBD;
import Controlador.ControlPersonal;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import Modelo.Personal;

public class frmLeerPersonal extends javax.swing.JFrame {

    public String codigo;
    private FileInputStream fisfoto;
    private int longitudBytes;
    private ControlPersonal conPers;
    private Personal pers;

    public frmLeerPersonal() {
        initComponents();
        conPers = new ControlPersonal();
        this.setLocationRelativeTo(null);
        jTablePersonal.setModel(new ModelTablePersonal(conPers.getPersonal()));
        jTablePersonal.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (jTablePersonal.getSelectedRow() != -1) {
                    int fila = jTablePersonal.getSelectedRow();
                    pers = ((ModelTablePersonal) jTablePersonal.getModel()).getFila(fila);
                    codigo = jTablePersonal.getValueAt(fila, 0).toString();
                    txtNombres.setText(pers.getNombre());
                    txtApellidos.setText(pers.getApelldos());
                    txtEmail.setText(pers.getEmail());
                    CustomImageIcon foto = ConexionBD.getFoto(Integer.parseInt(codigo));
                    if (foto != null) {
                        lblImgFoto.setIcon(foto);
                    } else {
                        lblImgFoto.setIcon(new CustomImageIcon(getClass().getResource("/resource/SinFoto.png")));
                    }
                    lblImgFoto.updateUI();
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bntActualizar = new javax.swing.JButton();
        bntEliminar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        bntNuevo = new javax.swing.JButton();
        bntGrabar = new javax.swing.JButton();
        lblImgFoto = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTablePersonal = new javax.swing.JTable();
        txtNombres = new javax.swing.JTextField();
        txtApellidos = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Demo Imágenes [By Darwin Durand]");
        setType(java.awt.Window.Type.POPUP);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bntActualizar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bntActualizar.setForeground(new java.awt.Color(0, 102, 102));
        bntActualizar.setText("Actualizar");
        bntActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntActualizarActionPerformed(evt);
            }
        });
        getContentPane().add(bntActualizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 130, -1, -1));

        bntEliminar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bntEliminar.setForeground(new java.awt.Color(0, 102, 102));
        bntEliminar.setText("Eliminar");
        bntEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntEliminarActionPerformed(evt);
            }
        });
        getContentPane().add(bntEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 130, 80, -1));

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton1.setForeground(new java.awt.Color(0, 102, 102));
        jButton1.setText("Cancelar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 130, -1, -1));

        bntNuevo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bntNuevo.setForeground(new java.awt.Color(0, 102, 102));
        bntNuevo.setText("Nuevo");
        bntNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(bntNuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 130, 90, -1));

        bntGrabar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bntGrabar.setForeground(new java.awt.Color(0, 102, 102));
        bntGrabar.setText("Grabar");
        bntGrabar.setEnabled(false);
        bntGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntGrabarActionPerformed(evt);
            }
        });
        getContentPane().add(bntGrabar, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 130, 80, -1));

        lblImgFoto.setBackground(new java.awt.Color(255, 255, 255));
        lblImgFoto.setIcon(new CustomImageIcon(getClass().getResource("/resource/SinFoto.png")));
        lblImgFoto.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lblImgFoto.setPreferredSize(new java.awt.Dimension(100, 80));
        lblImgFoto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblImgFotoMouseClicked(evt);
            }
        });
        getContentPane().add(lblImgFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 10, 120, 100));

        jScrollPane2.setPreferredSize(new java.awt.Dimension(375, 200));

        jTablePersonal.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jTablePersonal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTablePersonal.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(jTablePersonal);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, 530, 190));

        txtNombres.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        getContentPane().add(txtNombres, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 20, 180, -1));

        txtApellidos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        getContentPane().add(txtApellidos, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 50, 220, -1));

        txtEmail.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        getContentPane().add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 80, 220, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Email:");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, -1, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Nombres:");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 20, -1, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Apellidos");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 50, -1, -1));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, 550, 10));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bntActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntActualizarActionPerformed
        if (pers != null) {
            pers.setNombre(txtNombres.getText());
            pers.setApelldos(txtApellidos.getText());
            pers.setEmail(txtEmail.getText());
            pers.setFoto(fisfoto);
            int opcion = conPers.actualzarPersonal(pers);
            if (opcion != 0) {
                JOptionPane.showMessageDialog(this, "Personal Actualizado");
                jTablePersonal.setModel(new ModelTablePersonal(conPers.getPersonal()));
                if (fisfoto != null) {
                    try {
                        fisfoto.close();
                    } catch (IOException ex) {
                        Logger.getLogger(frmLeerPersonal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                fisfoto = null;
            }
        } else {
            JOptionPane.showMessageDialog(this, "Primero seleccione un Personal");
        }
    }//GEN-LAST:event_bntActualizarActionPerformed

    private void bntEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntEliminarActionPerformed
        if (pers != null) {
            int eliminarCliente = conPers.eliminarPersonal(pers.getPrimaryKey());
            if (eliminarCliente != 0) {
                JOptionPane.showMessageDialog(this, "Personal Eliminado");
                jTablePersonal.setModel(new ModelTablePersonal(conPers.getPersonal()));
            }
        } else {
            JOptionPane.showMessageDialog(this, "Primero seleccione un Personal");
        }
    }//GEN-LAST:event_bntEliminarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.txtNombres.setText("");
        this.txtApellidos.setText("");
        this.txtEmail.setText("");
        lblImgFoto.setIcon(new CustomImageIcon(getClass().getResource("/resource/SinFoto.png")));
        this.fisfoto = null;
        this.longitudBytes = 0;
        pers = null;
        this.bntGrabar.setEnabled(false);
        bntNuevo.setEnabled(true);
        this.jTablePersonal.clearSelection();
        this.bntActualizar.setEnabled(true);
        this.bntEliminar.setEnabled(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void bntNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntNuevoActionPerformed
        this.txtNombres.setText("");
        this.txtApellidos.setText("");
        this.txtEmail.setText("");
        lblImgFoto.setIcon(new CustomImageIcon(getClass().getResource("/resource/SinFoto.png")));
        this.fisfoto = null;
        this.longitudBytes = 0;
        this.bntGrabar.setEnabled(true);
        bntNuevo.setEnabled(false);
        this.bntActualizar.setEnabled(false);
        this.bntEliminar.setEnabled(false);
        pers = null;
        this.jTablePersonal.clearSelection();
    }//GEN-LAST:event_bntNuevoActionPerformed

    private void bntGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntGrabarActionPerformed
        if (this.txtNombres.getText().isEmpty()
                || this.txtApellidos.getText().isEmpty()
                || this.txtEmail.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Todos los campos son "
                    + "obligatorios, exepto la foto");
            return;
        }

        boolean emailEncontrado = ConexionBD.existeEmail(this.txtEmail.getText());
        if (emailEncontrado) {
            JOptionPane.showMessageDialog(this, "el email " + this.txtEmail.getText()
                    + " ya existe ingrese otro email");
            return;
        }
        Personal cl = new Personal();
        cl.setNombre(txtNombres.getText());
        cl.setApelldos(txtApellidos.getText());
        cl.setEmail(txtEmail.getText());
        cl.setFoto(fisfoto);
        int opcion = conPers.insertPersonal(cl);

        if (opcion != 0) {
            try {
                JOptionPane.showMessageDialog(this, "Personal " + cl + " agregado");
                jTablePersonal.setModel(new ModelTablePersonal(conPers.getPersonal()));
                this.bntGrabar.setEnabled(false);
                bntNuevo.setEnabled(true);
                this.bntActualizar.setEnabled(true);
                this.bntEliminar.setEnabled(true);
                if (fisfoto != null) {
                    fisfoto.close();
                }
                fisfoto = null;
            } catch (IOException ex) {
                Logger.getLogger(frmLeerPersonal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_bntGrabarActionPerformed

    private void lblImgFotoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblImgFotoMouseClicked
        JFileChooser se = new JFileChooser();
        se.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int estado = se.showOpenDialog(null);
        if (estado == JFileChooser.APPROVE_OPTION) {
            try {

                fisfoto = new FileInputStream(se.getSelectedFile());
                this.longitudBytes = (int) se.getSelectedFile().length();

                Image icono = ImageIO.read(se.getSelectedFile()).getScaledInstance(lblImgFoto.getWidth(), lblImgFoto.getHeight(), Image.SCALE_DEFAULT);
                lblImgFoto.setIcon(new ImageIcon(icono));
                lblImgFoto.updateUI();

            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_lblImgFotoMouseClicked
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmLeerPersonal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmLeerPersonal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmLeerPersonal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmLeerPersonal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmLeerPersonal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bntActualizar;
    private javax.swing.JButton bntEliminar;
    private javax.swing.JButton bntGrabar;
    private javax.swing.JButton bntNuevo;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTablePersonal;
    private javax.swing.JLabel lblImgFoto;
    private javax.swing.JTextField txtApellidos;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtNombres;
    // End of variables declaration//GEN-END:variables
}
