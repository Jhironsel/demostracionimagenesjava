package Controlador;

import Conexion.ConexionBD;
import java.util.ArrayList;
import Modelo.Personal;

/**
 *
 * @author Darwin Durand
 */
public class ControlPersonal {

    public ArrayList<Personal> getPersonal() {
        return ConexionBD.getPersonal(Personal.SELECT_ALL_NO_FOTO);
    }

    public int insertPersonal(Personal cl) {
        return ConexionBD.grabarPersonal(cl);
    }

    public int actualzarPersonal(Personal cl) {
        return ConexionBD.actualizarPersonal(cl);
    }

    public int eliminarPersonal(Integer pk) {
        return ConexionBD.eliminarPersonal(pk);
    }
}
