package Modelo;

import java.io.FileInputStream;

/**
 *
 * @author Darwin Durand
 */
public class Personal {

    public static String SELECT_ALL = "SELECT * FROM Personal";
    public static String SELECT_ALL_NO_FOTO = 
            "SELECT Codigo, nombre, apellidos, email FROM Personal";
    public static String UPDATE_CON_FOTO = 
            "UPDATE Personal SET " + "nombre = ? ," + "apellidos = ?, " + 
            "email = ?, " + "foto = ? WHERE Codigo = ?";
    public static String UPDATE_SIN_FOTO = 
            "UPDATE  Personal SET " + "nombre = ? , " + "apellidos = ? , " + 
            "email = ? WHERE Codigo = ?";
    public static String INSERT_CON_FOTO = 
            "INSERT INTO Personal" + "(nombre, apellidos, email, foto) "
            + "VALUES(?, ?, ?, ?)";
    public static String INSERT_SIN_FOTO = 
            "INSERT INTO Personal" + 
            "(nombre, apellidos, email) VALUES(?, ?, ?)";
    public static String DELETE = "DELETE FROM Personal where Codigo = ?";
    private Integer primaryKey;
    private String nombres;
    private String apellidos;
    private String email;
    private FileInputStream foto;

    public Personal(Integer primaryKey, String nombres, String apellidos, 
            String email, FileInputStream foto) {
        this.primaryKey = primaryKey;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
        this.foto = foto;
    }

    public Personal(String nombres, String apellidos, String email, 
            FileInputStream foto) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
        this.foto = foto;
    }

    public Personal() {
    }

    public Integer getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(Integer primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getNombre() {
        return nombres;
    }

    public void setNombre(String nombre) {
        this.nombres = nombre;
    }

    public String getApelldos() {
        return apellidos;
    }

    public void setApelldos(String apelldos) {
        this.apellidos = apelldos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public FileInputStream getFoto() {
        return foto;
    }

    public void setFoto(FileInputStream foto) {
        this.foto = foto;
    }

    @Override
    public String toString() {
        return nombres + apellidos;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (this.primaryKey != null ? this.primaryKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Personal other = (Personal) obj;
        if (this.primaryKey != other.primaryKey && 
                (this.primaryKey == null || 
                !this.primaryKey.equals(other.primaryKey))) {
            return false;
        }
        return true;
    }
}
